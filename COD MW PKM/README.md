# Call of Duty Modern Warfare PKM

Port of the PKM from MW 2019

## Requirements

Extended Weapon Skeleton
Extended Weapon Systems ESM

Both available on this same GitLab

## Installation

Install using a mod organizer or simply drag the folder into Fallout4\Data\

## License

Please do not reupload this mod or any of this assets without permission